# msg

a tiny static site generator

## usage

create a `_config.yaml` file in a directory (any directory!)  
then, create a `data` directory and add `.md` files in there.  
when you're done, run this program to output a tiny website in `build/` :]

all `.md` files are converted to HTML.
any other files not starting with `_` or `.` are copied directly into the build directory.

### example config

```yaml
name: this is a website
navigation:
  - name: home
    url: index.html
  - name: about
    url: about.html
```

## license

licensed under the 3-clause BSD license, found in the `LICENSE` file in the root of this repository.
